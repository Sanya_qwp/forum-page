$menu = $('.menu');
$owlSlider = $('.owl-carousel');
$hiddenText = $('.hidden-text');
$mobileMenu = $('.mobile-menu');
$mobileSlider = $('.slider_mobile');
$siteHat = $('.site-hat');
$siteFooter = $('.footer');
$menuPoint = $('.menu__point');
$goodPriceCond = $('.goods__price, .goods__conditions');
$desktopSlider = $('.slider_desktop');
$desktopSearchPopup =  $('.search-popup_desktop');
$productTitle = $('.products-info__title');

function footerFix(){
    if($(window).width() < 1170){
        $bodyHeight = $('body').height() - $siteFooter.height() + $siteHat.height();
        $siteFooter.css('top', $bodyHeight);
    }
}
function mobileInit(){
    if ($(window).width() < 1170){
        $menuPoint.removeClass('menu__point_active');
    }
    else{
        $('main').css('padding-bottom', $('footer').height());
        $desktopSlider.show();
        footerFix();
    }
}
$('#searchButton').on('click', function(){
    $desktopSearchPopup.hasClass('search-popup_active') ?
        $desktopSearchPopup
            .removeClass('search-popup_active')
            .hide()
    :
        $desktopSearchPopup
            .addClass('search-popup_active')
            .hide()
});
$productTitle.on('click', function(){
    $(this).hasClass('active') ?
        $(this)
            .removeClass('active')
            .siblings().hide() :
        $(this)
            .addClass('active')
            .siblings().show();
    footerFix();
});
$('#expandText').on('click', function(){
    $(this).hide();
    $hiddenText.show();
    $('#minimizeText').show();    
    footerFix();
});
$('#minimizeText').on('click', function(){
    $(this).hide();
    $hiddenText.hide();
    $('#expandText').show();
    footerFix();
});
$('#burgerMenuButton').on('click', function(){
    if ($mobileMenu.hasClass('mobile-menu_active')){
        $mobileMenu.removeClass('mobile-menu_active');
        $siteHat.removeClass('site-hat_active');
        $('main').removeClass('nonActive');
        $siteFooter.removeClass('nonActive');
    }
    else{
        $mobileMenu.addClass('mobile-menu_active');
        $siteHat.addClass('site-hat_active');
        $('main').addClass('nonActive');
        $siteFooter.addClass('nonActive');
    }
});
$(document).ready(function(){
    mobileInit();
    $menu
        .clone()
        .appendTo('.nav_mobile')
        .addClass('menu_mobile mobile');
    $owlSlider.owlCarousel({
        items: 1,
        nav: true,
        loop: true,
        autoplay: true,
        autoplayHoverPause: true
    });
    $('.products-info__chars-table, .hidden-text').hide();
    $('.menu__point').on('click', function(){
        $(this).hasClass('menu__point_active') ?
            $(this).removeClass('menu__point_active'):
            $(this).addClass('menu__point_active');
    });
    $owlSlider
        .clone()
        .appendTo('.slider-container_mobile')
        .addClass('slider_mobile mobile');
    $('.slider_mobile').owlCarousel({
        items: 1,
        nav: true,
        loop: true,
        autoplay: true,
        autoplayHoverPause: true
    });
    $goodPriceCond
        .clone()
        .appendTo('.goods')
        .addClass('mobile must-display');
});
$(window).resize(function(){
    mobileInit();
});